module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/blog your pathPrefix should be "blog"
  siteTitle: 'pear programming', // Navigation and Site Title
  siteTitleAlt: 'blog about code', // Alternative Site title for SEO
  siteTitleShort: 'blog about code', // short_name for manifest
  siteUrl: 'https://your-site.io', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  siteLogo: 'images/logo.png', // Used for SEO and manifest, path to your image you placed in the 'static' folder
  siteDescription: 'a blog about code',
  author: 'Alex Su', // Author for schemaORGJSONLD
  organization: '',

  // siteFBAppID: '123456789', // Facebook App ID - Optional
  userTwitter: '@alexytsu', // Twitter Username
  ogSiteName: 'pear programming', // Facebook Site Name
  ogLanguage: 'en_US',
  googleAnalyticsID: 'UA-142725545-1',

  // Manifest and Progress color
  themeColor: '#5348FF',
  backgroundColor: '#2b2e3c',

  // Social component
  twitter: 'https://twitter.com/alexytsu/',
  twitterHandle: '@alexytsu',
  github: 'https://github.com/alexytsu/',
  linkedin: '',
}
